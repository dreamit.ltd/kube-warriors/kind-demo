# Kind Cluster Demo

This repository has simple scripts to create two clusters using kind

1. Simple cluster - creates a single node cluster with no customizations
2. Ingress-ready cluster - creates a multi-node cluster and makes it ready to host ingress controller.

## How to run
Use the makefile to run make targets

```shell
make simple-cluster

make ingress-ready-cluster
```

## Deploy sample applications
To use below apps, you must have created ingress ready Kind cluster.

```
# Deploy nginx-ingress
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

# Application 1 - simple curl based app
kubectl apply -f https://kind.sigs.k8s.io/examples/ingress/usage.yaml

# Application 2 - simple webapp
kubectl apply -f ./bg-text-demo-app.yaml

```

simple-cluster:
	kind create cluster --config=./cluster.yaml --image kindest/node:v1.23.0

ingress-ready-cluster:
	kind create cluster --config=./cluster-nodeport.yaml --image kindest/node:v1.23.0

deploy-nginx-ingress:
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

deploy-test-apps:
	kubectl apply -f https://kind.sigs.k8s.io/examples/ingress/usage.yaml
	kubectl apply -f ./bg-text-demo-app.yaml
